//
//  function.swift
//  Laundry
//
//  Created by appentus on 9/13/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

extension UIViewController{
func push_VC(vc_name: String, storybord_name : String) {
    let story = UIStoryboard(name: storybord_name, bundle: nil)
    let vc = story.instantiateViewController(withIdentifier: vc_name)
    vc.modalPresentationStyle = .fullScreen
    self.present(vc, animated: true , completion: nil)
}

func dismiss_VC() {
    self.dismiss(animated: true, completion: nil)
}

}

extension UIView{
    func show_loader(){
        self.isUserInteractionEnabled = false
        SVProgressHUD.show()
    }
    func show_loader_progreess(prog: Float, mesg: String) {
        self.isUserInteractionEnabled = false
        SVProgressHUD.showProgress(prog, status: mesg)
    }
    func show_loader_stat(stat : String){
        self.isUserInteractionEnabled = false
        SVProgressHUD.show(withStatus: stat)
    }
    func hide_loader(){
        self.isUserInteractionEnabled = true
        SVProgressHUD.dismiss()
    }
    func show_loadar_success(title: String) {
        SVProgressHUD.showSuccess(withStatus: title)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            SVProgressHUD.dismiss()
            self.isUserInteractionEnabled = true
            
        }
    }
    func show_loadar_error(title: String) {
        SVProgressHUD.showError(withStatus: title)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            SVProgressHUD.dismiss()
            self.isUserInteractionEnabled = true
            
        }

    }
}
