//
//  login_VC.swift
//  Laundry
//
//  Created by appentus on 9/10/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import MRCountryPicker

class login_VC: UIViewController, MRCountryPickerDelegate {

    @IBOutlet weak var number_TF: UITextFieldX!
    @IBOutlet weak var password_TF: UITextFieldX!
    @IBOutlet weak var country_TF: UITextFieldX!
    
    
    var myPickerView = MRCountryPicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myPickerView.countryPickerDelegate = self
        myPickerView.showPhoneNumbers = true
        myPickerView.setCountryByName("United Arab Emirates")
        pickUp(country_TF)
        password_TF.isSecureTextEntry = true
    }
    

    @IBAction func login_Btn(_ sender: Any) {
        if !((password_TF.text?.remove_white_string().count)! > 5){
            func_ShowHud_Error(with: "password must be greater than 6 characters.")
        }else if !((number_TF.text?.remove_white_string().count)! > 0){
            func_ShowHud_Error(with: "Enter valid mobile number.")
        }else{
            loginUser()
        }
    }
    
    @IBAction func sign_Up_BTn(_ sender: Any) {
        push_VC(vc_name: "sign_Up_VC", storybord_name: "Main")
    }
    
    @IBAction func phone_btn(_ sender: Any) {
    }
    @IBAction func whatsapp_Btn(_ sender: Any) {
    }
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        country_TF.text = phoneCode
    }
    
    func pickUp(_ textField : UITextField){
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView
    }
    
    func loginUser() {
        self.view.endEditing(true)
        func_ShowHud()
        
        let param = [
            "customer_mobile":"\(self.number_TF.text!)",
            "customer_password":"\(self.password_TF.text!)",
            "customer_device_type":"2",
            "customer_device_token":fcm_token,
        ]
        APIFunc.postAPI(url: k_base_url+"customer_login", parameters: param) { (resp) in
            let status = return_status(resp)
            switch status{
            case .success:
                self.func_HideHud()
                let dict = resp["result"] as! NSArray
                let loginDict = dict[0] as! NSDictionary
                
                let data = NSKeyedArchiver.archivedData(withRootObject: loginDict)
                UserDefaults.standard.set(data, forKey: "loggedInUserData")
                loginModelData = try! jsonDecoder.decode(LoginModel.self, from: loginDict.createJsonData())
                self.push_VC(vc_name: "SideMenuController", storybord_name: "Main")
            case .fail:
                self.func_HideHud()
                self.func_ShowHud_Error(with: "\(resp["message"]!)")
                
            case .error_from_api:
                self.func_HideHud()
                self.func_ShowHud_Error(with: resp["error_message"] as! String)
            }
        }
    }
}
