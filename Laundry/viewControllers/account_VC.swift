//
//  account_VC.swift
//  Laundry
//
//  Created by appentus on 9/12/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class account_VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func back(_ sender: Any) {
      dismiss(animated: true, completion: nil)
    }
    
    @IBAction func edit_btn(_ sender: Any) {
        push_VC(vc_name: "profile_Edit_VC", storybord_name: "Main")
    }
    
    @IBAction func phone_btn(_ sender: Any) {
    }
    
    @IBAction func whatapp(_ sender: Any) {
    }

    @IBAction func addressBtn(_ sender: Any) {
        push_VC(vc_name: "address_Book_VC", storybord_name: "Main")
    }
    
    @IBAction func order_btn(_ sender: Any) {
        push_VC(vc_name: "order_List_VC", storybord_name: "Main")

    }
    
}
