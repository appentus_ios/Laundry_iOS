//
//  order_List_VC.swift
//  Laundry
//
//  Created by appentus on 9/12/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class order_List_VC: UIViewController {

    @IBOutlet weak var tbl_View: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tbl_View.delegate = self
        tbl_View.dataSource = self
    }
    
    @IBAction func back(_ sender: Any) {
        dismiss_VC()
    }
    
    @IBAction func phone_btn(_ sender: Any) {
    }
    
    @IBAction func btn_whtsapp(_ sender: Any) {
    }
    
}

extension order_List_VC: UITableViewDelegate{
    
}

extension order_List_VC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "order_list_TableCell", for: indexPath) as! order_list_TableCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
}
