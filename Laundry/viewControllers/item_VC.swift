//
//  item_VC.swift
//  Laundry
//
//  Created by appentus on 9/13/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class item_VC: UIViewController {

    @IBOutlet weak var tbl_View: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tbl_View.delegate = self
        tbl_View.dataSource  = self
    
    }
    

    @IBAction func back(_ sender: Any) {
        dismiss_VC()
    }
    
    @IBAction func phone(_ sender: Any) {
    }
    @IBAction func whatapp(_ sender: Any) {
    }
    @IBAction func bucket_btn(_ sender: Any) {
        push_VC(vc_name: "bucket_VC", storybord_name: "Main")
    }
}


extension item_VC: UITableViewDelegate{
    
}

extension item_VC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "item_TableCell", for: indexPath) as! item_TableCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
}
