//
//  sign_Up_VC.swift
//  Laundry
//
//  Created by appentus on 9/10/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import MRCountryPicker

class sign_Up_VC: UIViewController, MRCountryPickerDelegate {
   

    @IBOutlet weak var name_TF: UITextField!
    @IBOutlet weak var email_TF: UITextField!
    @IBOutlet weak var mobile_TF: UITextField!
    @IBOutlet weak var pass_TF: UITextField!
    @IBOutlet weak var area_TF: UITextField!
    @IBOutlet weak var country_TF: UITextField!
    @IBOutlet weak var cityTf: UITextFieldX!
    
    var picker = MRCountryPicker()
    var areaPicker = UIPickerView()
    
    var arrName : [String] = []
    var arrAreaDict : [NSDictionary] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAreaList()
        pass_TF.isSecureTextEntry = true
        mobile_TF.keyboardType = .numberPad
        email_TF.keyboardType = .emailAddress
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountryByName("United Arab Emirates")
        self.picker.backgroundColor = UIColor.white
        country_TF.inputView = self.picker
        area_TF.inputView = areaPicker
        areaPicker.delegate = self
        areaPicker.dataSource = self
    }
    
    @IBAction func back_btn(_ sender: Any) {
        dismiss_VC()
    }
    
    @IBAction func phonebtn(_ sender: Any) {
    }
    @IBAction func whatsapp_btn(_ sender: Any) {
    }
    @IBAction func finish_btn(_ sender: Any) {
        
        validate()
    }
    
    func validate(){
        if name_TF.text == "" {
            SVProgressHUD.showError(withStatus: "Please Enter Name")
        } else if email_TF.text == "" {
            SVProgressHUD.showError(withStatus: "Please Enter Email")
        }else if mobile_TF.text == "" {
            SVProgressHUD.showError(withStatus: "Please Enter Mobile Number")
        }else if pass_TF.text == "" {
            SVProgressHUD.showError(withStatus: "Please Enter Password")
        }else if area_TF.text == "" {
            SVProgressHUD.showError(withStatus: "Please select Area")
        } else if !isValidEmail(testStr: email_TF.text!){
            SVProgressHUD.showError(withStatus: "Please Enter Valid Email")
        }  else if cityTf.text! == ""{
            SVProgressHUD.showError(withStatus: "Please Enter Valid city")
        }else {
            
            signUp_API(name: name_TF.text!, email: email_TF.text!, mobile: mobile_TF.text!, pass: pass_TF.text!, city: cityTf.text!,area:self.area_TF.text!)
        }
    }
    
    func signUp_API(name:String , email:String, mobile: String, pass:String, city:String,area:String){
        
        
        var idArea = ""
        let index = arrName.firstIndex(of: area)
        idArea = "\(self.arrAreaDict[index!]["area_location_id"]!)"

            
        self.view.show_loader()
        let url = k_base_url+"customer_registration"
        let para = ["customer_name": name,
                    "customer_country_code": "+91",
                    "customer_mobile": mobile,
                    "customer_email": email,
                    "customer_city": city,
                    "customer_password": pass,
                    "customer_device_type":"2",
                    "customer_device_token": fcm_token,
                    "area_id": "\(idArea)"]
            as [String:Any]
        
        APIFunc.postAPI(url: url, parameters: para) { (response) in
        
            let status = "\(response["status"]!)"
            let message = "\(response["message"]!)"
            
            if status == "success"{
                self.view.hide_loader()
                self.view.show_loadar_success(title: "Signup Successfull")
                let dict = response["result"] as! NSArray
                let loginDict = dict[0] as! NSDictionary
                
                let data = NSKeyedArchiver.archivedData(withRootObject: loginDict)
                UserDefaults.standard.set(data, forKey: "loggedInUserData")
                loginModelData = try! jsonDecoder.decode(LoginModel.self, from: loginDict.createJsonData())
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.8){
                    self.push_VC(vc_name: "home_VC", storybord_name: "Main")
                }
                
            } else {
                self.view.hide_loader()
                self.view.show_loadar_error(title: message)
                
            }
            
            
        }
    }
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        country_TF.text = phoneCode
    }
    
    func getAreaList() {
        self.view.endEditing(true)
        func_ShowHud()
        let param = ["":""]
        
        APIFunc.getAPI(url: k_base_url+"get_area_list", parameters: param) { (resp) in
            let status = return_status(resp)
            switch status{
            case .success:
                let resp_arr = resp["result"] as! NSArray
                self.arrAreaDict = []
                self.arrName = []
                for i in 0..<resp_arr.count{
                    let dict = resp_arr[i] as! NSDictionary
                    self.arrAreaDict.append(dict)
                    self.arrName.append("\(dict["area_location_name"]!)")
                }
                
                self.area_TF.text = "\(self.arrAreaDict[0]["area_location_name"]!)"
                self.func_HideHud()
            case .fail:
                self.func_HideHud()
                self.func_ShowHud_Error(with: "\(resp["message"]!)")
                
            case .error_from_api:
                self.func_HideHud()
                self.func_ShowHud_Error(with: resp["error_message"] as! String)
            }
        }
    }
    
}
extension sign_Up_VC:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrName.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arrName[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.area_TF.text = self.arrName[row]
    }
}
