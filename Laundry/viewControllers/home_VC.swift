//
//  home_VC.swift
//  Laundry
//
//  Created by appentus on 9/10/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit
import SideMenuSwift
import SDWebImage

class home_VC: UIViewController, SideMenuControllerDelegate {

    @IBOutlet weak var welcome_Lbl: UILabel!
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var imageArr = [String]()
    var lastIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.welcome_Lbl.text = "Hello " + (loginModelData?.customerName ?? "")
        sideMenuController?.delegate = self
        SideMenuController.preferences.basic.menuWidth = 260
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
        menu_api()
    }
    
    func menu_api() {
        self.view.endEditing(true)
        func_ShowHud()
        let param = ["":""]
        
        APIFunc.getAPI(url: k_base_url+"get_banner", parameters: param) { (resp) in
            let status = return_status(resp)
            switch status{
            case .success:
                self.imageArr = [String]()
                let resp_arr = resp["result"] as! NSArray
                
                for item in resp_arr{
                    let dict = item as! NSDictionary
                    self.imageArr.append("\(dict["banner_path"]!)")
                }
                self.pageControl.numberOfPages = self.imageArr.count
                self.collView.reloadData()
                self.startTimer()
                self.func_HideHud()
            case .fail:
                self.func_HideHud()
                self.func_ShowHud_Error(with: "\(resp["message"]!)")
                
            case .error_from_api:
                self.func_HideHud()
                self.func_ShowHud_Error(with: resp["error_message"] as! String)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = collView.contentOffset
        visibleRect.size = collView.bounds.size
        let visiblePoint = CGPoint(x: CGFloat(visibleRect.midX), y: CGFloat(visibleRect.midY))
        let visibleIndexPath: IndexPath? = collView.indexPathForItem(at: visiblePoint)
        self.pageControl.currentPage = visibleIndexPath!.row
    }
    
    @objc func scrollToNextCell(){
        var visibleRect = CGRect()
        visibleRect.origin = collView.contentOffset
        visibleRect.size = collView.bounds.size
        let visiblePoint = CGPoint(x: CGFloat(visibleRect.midX), y: CGFloat(visibleRect.midY))
        let visibleIndexPath: IndexPath? = collView.indexPathForItem(at: visiblePoint)
        
        if self.lastIndex == self.imageArr.count - 1{
            collView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
        }else{
            collView.scrollToItem(at: IndexPath(item: self.lastIndex+1, section: 0), at: .left, animated: true)
        }
        self.pageControl.currentPage = visibleIndexPath!.row
        self.lastIndex = visibleIndexPath!.row
    }

    func startTimer() {
        if self.imageArr.count > 1{
            _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)
            }
        }
        
    @IBAction func easy_pick_btn(_ sender: Any) {
    }
    
    @IBAction func selected_btn(_ sender: Any) {
        push_VC(vc_name: "item_VC", storybord_name: "Main")
    }
    @IBAction func side_menu_btn(_ sender: Any) {
         self.sideMenuController?.revealMenu()
    }
    
    @IBAction func phone_btn(_ sender: Any) {
    }
    
    @IBAction func whatsapp_btn(_ sender: Any) {
    }
}
extension home_VC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeBannerCell", for: indexPath)
        let imageView = cell.viewWithTag(1) as? UIImageView
        imageView?.sd_setImage(with: URL(string: imageArr[indexPath.row]))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageArr.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collView.frame.width, height: self.collView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
