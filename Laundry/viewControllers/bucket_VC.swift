//
//  bucket_VC.swift
//  Laundry
//
//  Created by appentus on 9/13/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class bucket_VC: UIViewController {

    @IBOutlet weak var tbl_View: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tbl_View.delegate = self
        tbl_View.dataSource = self
    }
    

    @IBAction func back(_ sender: Any) {
        dismiss_VC()
    }
    
}

extension bucket_VC: UITableViewDelegate{
    
}

extension bucket_VC: UITableViewDataSource{
  
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "bucket_TableCell", for: indexPath) as! bucket_TableCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "bucket_2_TableViewCell", for: indexPath) as! bucket_2_TableViewCell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 60
        } else {
            return 40
        }
        
    }
    
}
