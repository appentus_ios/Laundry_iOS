//
//  price_Listing_VC.swift
//  Laundry
//
//  Created by appentus on 9/13/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class price_Listing_VC: UIViewController {

    
    @IBOutlet weak var tbl_View: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbl_View.delegate = self
        tbl_View.dataSource = self
        
    }
    

    @IBAction func back(_ sender: Any) {
        dismiss_VC()
    }
    
}

extension price_Listing_VC: UITableViewDelegate{
    
}

extension price_Listing_VC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "price_List_TableCell", for: indexPath) as! price_List_TableCell
        
        return cell
    }
    
}
