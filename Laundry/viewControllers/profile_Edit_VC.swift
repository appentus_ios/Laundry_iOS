
//
//  profile_Edit_VC.swift
//  Laundry
//
//  Created by appentus on 9/12/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class profile_Edit_VC: UIViewController {

    @IBOutlet weak var name_TF: UITextField!
    @IBOutlet weak var email_TF: UITextField!
    @IBOutlet weak var city_TF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func back(_ sender: Any) {
        dismiss_VC()
    }
    
    @IBAction func update_btn(_ sender: Any) {
        push_VC(vc_name: "account_VC", storybord_name: "Main")
    }
}
