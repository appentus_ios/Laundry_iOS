//
//  add_Addres_VC.swift
//  Laundry
//
//  Created by appentus on 9/12/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class add_Addres_VC: UIViewController {

    @IBOutlet weak var address_TF: UITextField!
    @IBOutlet weak var flat_TF: UITextField!
    @IBOutlet weak var street_TF: UITextField!
    @IBOutlet weak var landmark_TF: UITextField!
    @IBOutlet weak var city_TF: UITextField!
    @IBOutlet weak var COUNTRY_TF: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

    @IBAction func done_Btn(_ sender: Any) {
        push_VC(vc_name: "address_Book_VC", storybord_name: "Main")
    }
    
    @IBAction func back(_ sender: Any) {
        dismiss_VC()
    }
    
}
