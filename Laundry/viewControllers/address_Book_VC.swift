//
//  address_Book_VC.swift
//  Laundry
//
//  Created by appentus on 9/12/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class address_Book_VC: UIViewController {

    @IBOutlet weak var tbl_view: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbl_view.delegate = self
        tbl_view.dataSource = self
    }
    


    @IBAction func back(_ sender: Any) {
        dismiss_VC()
    }
    
    @IBAction func phone_btn(_ sender: Any) {
    }
    
    @IBAction func whatsapp_btn(_ sender: Any) {
    }
    
    @IBAction func add_btn(_ sender: Any) {
        push_VC(vc_name: "add_Addres_VC", storybord_name: "Main")
    }
}

extension address_Book_VC: UITableViewDelegate{
    
}

extension address_Book_VC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "address_book_TableCell", for: indexPath) as! address_book_TableCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
}
