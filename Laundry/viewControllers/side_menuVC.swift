//
//  side_menuVC.swift
//  Laundry
//
//  Created by appentus on 9/10/19.
//  Copyright © 2019 appentus. All rights reserved.
//

import UIKit

class side_menuVC: UIViewController {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var lblRemainingAmount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameLbl.text = loginModelData?.customerName
        self.lblRemainingAmount.text = loginModelData?.customerRemainingAmount
    }
    

    @IBAction func selection_BTn(_ sender: UIButton) {
        self.sideMenuController?.hideMenu()
        
        if sender.tag == 0{
            self.sideMenuController?.hideMenu()
        } else  if sender.tag == 1{
             self.sideMenuController?.hideMenu()
            let nav = storyboard?.instantiateViewController(withIdentifier: "account_VC") as! account_VC
            self.present(nav, animated: true, completion: nil)
//            push_VC(vc_name: "account_VC", storybord_name: "Main")
        } else  if sender.tag == 2{
            push_VC(vc_name: "order_List_VC", storybord_name: "Main")
        } else  if sender.tag == 3{
            push_VC(vc_name: "price_Listing_VC", storybord_name: "Main")
        } else  if sender.tag == 4{
//            push_VC(vc_name: <#T##String#>, storybord_name: "Main")
        } else  if sender.tag == 5{
            push_VC(vc_name: "policy_VC", storybord_name: "Main")
        } else  if sender.tag == 6{
            push_VC(vc_name: "privacy_policy_VC", storybord_name: "Main")
        } else  {
            var refreshAlert = UIAlertController(title: "Alert", message: "All data will be lost.Are you sure you want to logout?", preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
                self.push_VC(vc_name: "login_VC", storybord_name: "Main")
              }))

            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
              }))

            present(refreshAlert, animated: true, completion: nil)
        }
        
    }
    
}
