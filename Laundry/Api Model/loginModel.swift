//
//  loginModel.swift
//  Laundry
//
//  Created by Ayush Pathak on 23/02/20.
//  Copyright © 2020 appentus. All rights reserved.
//

import Foundation


// MARK: - LoginModel
class LoginModel: Codable {
    let customerID, customerName, customerEmail, customerMobile: String
    let customerPassword, customerPasswordString, customerCountryCode, customerProfile: String
    let customerCity, customerDeviceType, customerDeviceToken, insertDate: String
    let customerRemainingAmount, customerAreaID: String

    enum CodingKeys: String, CodingKey {
        case customerID = "customer_id"
        case customerName = "customer_name"
        case customerEmail = "customer_email"
        case customerMobile = "customer_mobile"
        case customerPassword = "customer_password"
        case customerPasswordString = "customer_password_string"
        case customerCountryCode = "customer_country_code"
        case customerProfile = "customer_profile"
        case customerCity = "customer_city"
        case customerDeviceType = "customer_device_type"
        case customerDeviceToken = "customer_device_token"
        case insertDate = "insert_date"
        case customerRemainingAmount = "customer_remaining_amount"
        case customerAreaID = "customer_area_id"
    }

    init(customerID: String, customerName: String, customerEmail: String, customerMobile: String, customerPassword: String, customerPasswordString: String, customerCountryCode: String, customerProfile: String, customerCity: String, customerDeviceType: String, customerDeviceToken: String, insertDate: String, customerRemainingAmount: String, customerAreaID: String) {
        self.customerID = customerID
        self.customerName = customerName
        self.customerEmail = customerEmail
        self.customerMobile = customerMobile
        self.customerPassword = customerPassword
        self.customerPasswordString = customerPasswordString
        self.customerCountryCode = customerCountryCode
        self.customerProfile = customerProfile
        self.customerCity = customerCity
        self.customerDeviceType = customerDeviceType
        self.customerDeviceToken = customerDeviceToken
        self.insertDate = insertDate
        self.customerRemainingAmount = customerRemainingAmount
        self.customerAreaID = customerAreaID
    }
}
